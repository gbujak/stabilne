#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <math.h>

#include <allegro5/allegro.h>
//#include <allegro5/allegro_font.h>
//#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>

#define SWIDTH  800
#define SHEIGHT 700
#define RADIUS  20
#define MARGIN  50
#define PI 3.14159265358979323846

//ALLEGRO_FONT* font;
ALLEGRO_DISPLAY* display;

/**
 * Struktura person opisuje osobę.
 * Pole id zawiera indeks osoby w liście osób.
 * Wskaźnik na kolejną strukturę person jest w programie ustawiany
 * na konkubenta osoby w przypadku instnienia związku.
 * Pref to tabela liczb stałych będących indeksami osób,
 * które preferuje opisywana osoba. Osoby o mniejszym indeksie
 * są preferowane nad tymi o wyższym.
 */
struct person {
    int id;
    struct person *marr; //partner
    int *pref; //prefers
};

struct point {
    unsigned int x, y;
};

struct line {
    struct point A, B;
};

/**
 * funkcja oblicza odpowiednie położenie koła reprezentującego osobę na ekranie
 */
struct point get_pos(bool male, unsigned index, int size) {
    unsigned gap = (SHEIGHT - (size * RADIUS*2)) / (size + 1);
    unsigned x = (male != true) ? MARGIN : SWIDTH - MARGIN;
    return (struct point) {
        x, (1 + index) * gap + index * (RADIUS*2) + RADIUS
    };
}

/**
 * funkcja zwraca linię łączącą koła reprezentujące osoby na ekranie
 */
struct line get_line(unsigned m_index, unsigned w_index, int size) {
    struct point m_point = get_pos(true,  m_index, size);
    struct point w_point = get_pos(false, w_index, size);
    
    return (struct line) { m_point, w_point };
}

/**
 * funkcja rysuje na ekranie koła reprezentujące osoby i linie 
 * reprezentujące małżeństwa
 */
void draw_all(struct person* men, struct person* women, int size) {
    struct point p;
    struct line l;
    al_clear_to_color(al_map_rgb(50, 50, 50));
    for (int i = 0; i < size; i++) {
        if (men[i].marr != NULL){
            l = get_line(i, men[i].marr->id, size);
            al_draw_line(
                l.A.x, l.A.y, l.B.x, l.B.y,
                al_map_rgb(255, 255, 255), 2
            );
        }
        p = get_pos(true, i, size);
        // printf("drawing man %d at: %d, %d\n", i, p.x, p.y);
        // al_draw_circle(p.x, p.y, RADIUS, al_map_rgb(155, 255, 255), 2);
        al_draw_triangle(
            p.x, p.y + RADIUS * 1.2, p.x - RADIUS, p.y - RADIUS / 1.5, p.x + RADIUS, p.y - RADIUS / 1.5,
            al_map_rgb(155, 255, 255), 2
        );
    }
    for (int i = 0; i < size; i++) {
        p = get_pos(false, i, size);
        al_draw_circle(p.x, p.y, RADIUS, al_map_rgb(255, 155, 255), 2);
    }
    al_flip_display();
}

void draw_wait(struct person* men, struct person* women, int size) {
    draw_all(men, women, size);
    al_rest(0.3);
}

/**
 * Funkcja shuffle dostaje tabelę liczb całkowitych i rozmiar tabeli.
 * Losowo zmienia wartości w tabeli. Jest używana do wygenerowania
 * losowej listy preferencji dla każdej z osób.
 */
void shuffle(int *array, int size) {
    for (int i = size - 1; i > 0; --i) {
        int j = rand() % (i + 1);
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
}

/**
 * Funkcja rand_group tworzy grupę osób o rozmiarze "size"
 * każda osoba w grupie preferuje "size" osób w losowej kolejności.
 */
struct person *rand_group(int size) {
    struct person *g = calloc(size, sizeof(struct person));
    for (int i = 0; i < size; i++) {
        g[i].id = i;
        g[i].marr = NULL;
        g[i].pref = calloc(size, sizeof(int));
        for (int j = 0; j < size; j++)
            g[i].pref[j] = j;
        shuffle(g[i].pref, size);
    }
    return g;
}

/**
 * Funkcja print_group drukuje grupy osób w formacie:
 * id_osoby->id_partnera:lista, preferencji, osoby
 */
void print_group(struct person *g, int size) {
    for (int i = 0; i < size; i++) {
        printf("%d->",g[i].id);
        if (g[i].marr != NULL) printf("%d", g[i].marr->id);
        printf(":");
        for (int j = 0; j < size; j++)
            printf("%d,", g[i].pref[j]);
        putc('\n', stdout);
    }
}

/**
 * free_group zwalnia dynamicznie alokowaną listę preferencji
 * każdej osoby w grupie, a potem zwania samą grupę, która też
 * jest dynamicznie alokowana
 */
void free_group(struct person *g, int size) {
    printf("freeing: "); 
    for (int i = 0; i < size; i++) {
        printf("%d, ", i);
        free(g[i].pref);
    }   
    free(g);
    puts("");
}

/**
 * funkcja divorce łamie związek osoby, na którą wskaźnik poda się jej
 * jako argument. Przerywa program, gdy osoba nie jest w związku.
 */
void divorce(struct person* p) {
    if (p->marr == NULL) {
        puts("divorcing unmarried");
        abort();
    }
    p->marr->marr = NULL;
    p->marr = NULL;
}

/**
 * funkcja marry ustanawia małżeństwo. przerywa program, gdy jedna z osób
 * jest już w zwiąku.
 */
void marry(struct person *p, struct person *p2) {
    if (p->marr != NULL || p2->marr != NULL) {
        puts("double marriage");
        abort();
    }
    p  -> marr = p2;
    p2 -> marr = p;
}

/**
 * \file
 * Funkcja favorite dostaje wskaźnik na osobę i dwa indeksy osób.
 * Funkcja sprawdza, która z osób partner jest pierwsza w tabeli
 * preferencji osoby p. zwraca indeks osoby bardziej pożądanej. 
 * liczba size jest używana do bezpiecznego iterowania po tabeli.
 */
int favorite(struct person* p, int partner1, int partner2, int size) {
    for (int i = 0; i < size; i++)
        if (p->pref[i] == partner1 || p->pref[i] == partner2)
            return p->pref[i];
    puts("cant find favorite");
    abort();
}

/**
 * Funkcja stable_marriage dostaje wskaźniki na dwie grupy osób i ich rozmiar.
 * przeprowadza działania zgodnie ze schematem krokowym umieszczonym w sprawozdaniu.
 * 
 * Dopóki istnieją kobiety bez związku, iteruje po tabeli kobiet, pomijając iterację,
 * gdy badana kobieta jest w związku. Jeśli tak nie jest, iteruje po tabeli mężczyzn.
 * Funkcja przerywa iterację, gdy dojdzie do końca tabeli (co teoretycznie nie powinno
 * nastąpić) lub gdy znajdzie mężczyznę bez związku lub w związku, ale preferującego
 * badaną kobietę od tej, z którą jest w związku. 
 * 
 * Gdy funkcja znajdzie jedną z opisanych sytuacji przerywa związek mężczyzny i żeni
 * go z badaną kobietą, lub po prostu żeni, gdy mężczyzna nie jest w związku. Funkcja
 * dekrementuje liczbę kobiet bez związku tylko wtedy, gdy mężczyzna nie miał wcześniej
 * żony. 
 */
void stable_marriage(struct person *women, struct person *men, int size) {
    /// zmienna przechowuje liczbę kobiet nie będących w związku
    int maiden_count = size;

    while(maiden_count != 0) {
        for (int w = 0; w < size; w++) {
            if (women[w].marr != NULL) continue;
            for (int m = 0; m < size; m++) {
                struct person* current_man = &men[ women[w].pref[m] ];
                // printf("-> %lu, %d\n", (unsigned long) current_man, current_man->id);
                if (current_man->marr == NULL) {
                    printf("marrying woman %d with man %d, man is unmarried\n", w, m);
                    marry(&women[w], current_man);
                    maiden_count--;
                    draw_wait(men, women, size);
                    break;
                } else if (favorite(current_man, current_man->marr->id, w, size) == w) {
                    printf(
                        "marrying woman %d with man %d, breaking marriage with woman %d\n",
                        w, m, current_man->marr->id
                    );
                    divorce (current_man);
                    marry   (current_man, &women[w]);
                    draw_wait(men, women, size);
                    break;
                }
            }
        }
    }
}

void init() {
    al_init();

    al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
    al_set_new_display_option(ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST);

    display = al_create_display(SWIDTH, SHEIGHT);
    //al_init_font_addon();
    //al_init_ttf_addon();
    al_init_primitives_addon();

    //font = al_load_font("ubuntu.ttf", 24, 0);

    al_clear_to_color(al_map_rgb(50, 50, 50));

    al_flip_display();
}

void readline(FILE* f, char* buffer) {
    char in = fgetc(f);
    while (in != '\n' && !feof(f)) {
        *buffer = in;
        buffer++;
        in = fgetc(f);
    }
    *buffer = '\0';
}

/**
 * funkcja zczytuje grupy, których stabilne małżeństwo należy obliczyć
 * z pliku podanego obok nazwy skompilowanego programu
 */
int read_group(struct person** men_ptr, struct person** women_ptr, const char* fname) {
    FILE* f = fopen(fname, "r");
    char buffer[4000] = {'\0'};
    char tokbuff[4000] = {'\0'};
    int size;

    readline(f, buffer);
    size = atoi(buffer);

    struct person *g = calloc(size, sizeof(struct person));
    for (int i = 0; i < size; i++) {

        readline(f, buffer);
        strcpy(tokbuff, buffer);
        char* token = strtok(tokbuff, ",");

        g[i].id = i;
        g[i].marr = NULL;
        g[i].pref = calloc(size, sizeof(int)); 

        g[i].pref[0] = atoi(token);
        for (int j = 1; j < size; j++) {
            token = strtok(NULL, ",");
            if (token == NULL) {
                printf("too few preferences in file for woman %d !!!\n", i);
                abort();
            } else if (atoi(token) >= size) {
                printf("preference bigger than size for woman %d !!!\n", i);
                abort();
            }
            g[i].pref[j] = atoi(token);
        }
    }
    
    *women_ptr = g;

    readline(f, buffer);

    g = calloc(size, sizeof(struct person));
    for (int i = 0; i < size; i++) {
        readline(f, buffer);
        puts(buffer);
        strcpy(tokbuff, buffer);
        char* token = strtok(tokbuff, ",");

        g[i].id = i;
        g[i].marr = NULL;
        g[i].pref = calloc(size, sizeof(int));

        g[i].pref[0] = atoi(token);
        for (int j = 1; j < size; j++) {
            token = strtok(NULL, ",");
            if (token == NULL) {
                printf("too few preferences in file for man %d !!!\n", i);
                abort();
            } else if (atoi(token) >= size) {
                printf("preference bigger than size for man %d !!!\n", i);
                abort();
            }
            g[i].pref[j] = atoi(token);
        }
    }
    puts("test");
    
    print_group(g, size);
    *men_ptr = g;
    fclose(f);
    return size;
}

int main(int argc, char **argv) {
    init();
    srand(time(NULL));

    struct person *men = NULL;
    struct person *women = NULL;
    int size = 0;

    if (argc == 1) {
        size = 10;
        men = rand_group(size);
        women = rand_group(size);
    } else {
        size = read_group(&men, &women, argv[1]);
    }

    draw_all(men, women, size);
    al_rest(2.0);

    stable_marriage(women, men, size);

    puts("");
    puts("women:");
    print_group(women, size);
    puts("men:");
    print_group(men, size);

    fgetc(stdin);

    puts("");

    puts("men:");
    free_group(men, size);
    puts("women:");
    free_group(women, size);

    al_destroy_display(display);
}
