# Projekt  - algorytmy i struktury danych

# Wstęp teoretyczny (teoria grafów)
![grafika 1](https://d2mxuefqeaa7sj.cloudfront.net/s_79E542C106A699F7D343B25A24934C9CB0EAE5F12C753D702BB541F674F7F484_1551466434634_dwa_grafy.svg)


**Graf dwudzielny** to graf,  którego zbiór wierzchołków można podzielić na dwa zbiory, których część wspólna jest pustym zbiorem oraz krawędzie nie łączą wierzchołków tego samego zbioru. 

Na grafice 1:
$$V = \{v_1, v_2, v_3, v_4, v_5\}$$
$$V_1 = \{v_1, v_2\}$$
$$V_2 = \{v_3, v_4, v_5\}$$
$$V_1 \cap V_2 = \emptyset$$

## Twierdzenie o kojarzeniu małżeństw (twierdzenie Halla)

Twierdzenie to łatwo jest sobie wyobrazić za pomocą dwóch grup: kobiet i mężczyzn o tej samej liczbie członków. Dla każdej kobiety przyporządkowany jest zbiór mężczyzn, których chciałaby poślubić. Mężczyźni chętnie ożenią się z każdą kobietą, która się na to zgodzi. Twierdzenie mówi, że, gdy spełnione są pewne warunki, da się połączyć członków grup w pary, które stworzą szczęśliwe małżeństwa. 

*Warunkiem opisywanym przez twierdzenie jest to, by każdy podzbiór kobiet liczących k osób chciał poślubić co najmniej k mężczyzn.*


![grafika 2](https://d2mxuefqeaa7sj.cloudfront.net/s_79E542C106A699F7D343B25A24934C9CB0EAE5F12C753D702BB541F674F7F484_1551476195668_polaczone.svg)
![grafika 3](https://d2mxuefqeaa7sj.cloudfront.net/s_79E542C106A699F7D343B25A24934C9CB0EAE5F12C753D702BB541F674F7F484_1551476195660_nie_da_sie.svg)


Na powyższych grafikach łatwo zauważyć potwierdzenie twierdzenia. Na grafice 2 warunek jest spełniony i istnieje połączenie osób w pary. Na grafice 3 dwie kobiety chcą tylko jednego mężczyzny, przez co jedna z kobiet oraz jeden z mężczyzn zostaną bez pary.

# Przykładowe zastosowanie algorytmu

Aby lepiej zrozumieć działanie algorytmu, warto przyjrzeć się przykładowym danym początkowym oraz wynikowi działania algorytmu. Przedstawmy poszczególne kobiety literami alfabetu a mężczyzn cyframi. Algorytm musi dostać listy preferencji każdej osoby, co przykładowo wygląda tak:

| A: 3, 2, 1, 4 | 1: C, D, A, B |
| ------------- | ------------- |
| B: 2, 3, 1, 4 | 2: B, A, C, D |
| C: 3, 1, 4, 2 | 3: C, A, B, D |
| D: 3, 1, 2, 4 | 4: C, A, D, B |


Poniżej opisany jest każdy z kroków, jakie musi podjąć algorytm w celu zwrócenia poprawnego wyniku (wybór kobiet i mężczyzn w kolejności występowania w listach):


1. wybranie kobiety
2. wybranie pierwszego mężczyzny z listy preferencji badanej kobiety
3. sprawdzenie, czy ten jest już w związku *(a - prawda; b - fałsz)*
    1. sprawdzenie, czy badana kobieta jest wyżej na liście preferencji mężczyzny niż ta, z którą jest aktualnie w związku *(i - prawda; ii - fałsz)*
        1. złamanie związku i ustanowienie związku badanej kobiety i badanego mężczyzny
        2. powrót do punktu 3. dla kolejnego mężczyzny na liście preferencji kobiety
    2. ustanowienie związku badanej kobiety i badanego mężczyzny
4. Sprawdzenie, czy badana kobieta była ostatnią z kobiet
    1. koniec algorytmu, zwrócenie wyniku
    2. powrót do punktu 2. dla kolejnej kobiety z listy kobiet


## Działanie i wynik algorytmu dla ustalonych danych
| A: 3, 2, 1, 4 | 1: C, D, A, B |
| ------------- | ------------- |
| B: 2, 3, 1, 4 | 2: B, A, C, D |
| C: 3, 1, 4, 2 | 3: C, A, B, D |
| D: 3, 1, 2, 4 | 4: C, A, D, B |



    A3 – akceptowane;
    B2 – akceptowane;
    C3 – mężczyzna 3 zajęty, ale związek złamany, gdyż C wyżej na liście mężczyzny;
    A2 – mężczyzna 2 zajęty, związek nie złamany, gdyż B wyżej na liście mężczyzny;
    A1 – akceptowane;
    D3 – mężczyzna 3 zajęty, związek nie złamany;
    D1 – mężczyzna 1 zajęty, ale związek złamany;
    A4 – akceptowane;
    
    wynik -> A4, B2, C3, D1



----------
# Wypis programu na standardowe wyjście

Poszczególne osoby z grupy są wypisywane w formacie

    {id_osoby}->{id_partnera}:{lista, preferencji, osoby} 


    marrying woman 0 with man 0, man is unmarried
    marrying woman 1 with man 0, breaking marriage with woman 0
    marrying woman 2 with man 0, breaking marriage with woman 1
    marrying woman 3 with man 1, man is unmarried
    marrying woman 4 with man 0, breaking marriage with woman 2
    marrying woman 0 with man 2, man is unmarried
    marrying woman 1 with man 2, breaking marriage with woman 0
    marrying woman 2 with man 2, breaking marriage with woman 1
    marrying woman 0 with man 3, man is unmarried
    marrying woman 1 with man 3, breaking marriage with woman 0
    marrying woman 0 with man 4, man is unmarried
    
    women:
    0->4:2,3,1,0,4,
    1->3:3,1,0,4,2,
    2->2:2,1,3,0,4,
    3->1:2,0,1,4,3,
    4->0:0,1,2,4,3,
    men:
    0->4:4,2,1,0,3,
    1->3:3,1,0,4,2,
    2->2:2,3,1,0,4,
    3->1:1,3,4,0,2,
    4->0:2,1,4,3,0,
    
    men:
    freeing: 0, 1, 2, 3, 4, 
    women:
    freeing: 0, 1, 2, 3, 4, 


----------
## Kamień milowy 2

Do programu dodana została graficzna wizualizacja algorytmu wykonana za pomocą biblioteki allegro oraz możliwość wczytania grupy z pliku tekstowego

                                                        `a.out plik.txt`  
----------
# Dokumentacja wygenerowana w programie Doxygen















